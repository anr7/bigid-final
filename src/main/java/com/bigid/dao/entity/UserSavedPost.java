package com.bigid.dao.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author Satish
 *
 */
@Entity
@Table(name = "USER_SAVED_POST")
public class UserSavedPost extends BaseEntity{

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "ID")
	private long id;
	@Column(name="SAVED_BY")
	private Long userId;
	@Column(name = "POST_ID")
	private Long postId;
	@Column(name = "POST_STATUS")
	private String status;
	@Column(name = "POST_COUNT")
	private int postCount;
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public Long getUserId() {
		return userId;
	}
	public void setUserId(Long userId) {
		this.userId = userId;
	}
	public Long getPostId() {
		return postId;
	}
	public void setPostId(Long postId) {
		this.postId = postId;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public int getPostCount() {
		return postCount;
	}
	public void setPostCount(int postCount) {
		this.postCount = postCount;
	}
	@Override
	public String toString() {
		return "UserSavedPost [id=" + id + ", userId=" + userId + ", postId=" + postId + ", status=" + status
				+ ", postCount=" + postCount + "]";
	}
	
}
