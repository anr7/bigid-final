package com.bigid.dao.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="POST_REPORT")
public class PostReport {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="ID", nullable = false, unique = true)
	private int id;
	@Column(name="USER_ID")
	private long userId;
	@Column(name="POST_ID")
	private long postId;
	@Column(name="ACTIVITY")
	private String activity;
	@Column(name="ACTIVITY_TIME")
	private Date activityTime;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public long getUserId() {
		return userId;
	}
	public void setUserId(long userId) {
		this.userId = userId;
	}
	public long getPostId() {
		return postId;
	}
	public void setPostId(long postId) {
		this.postId = postId;
	}
	public String getActivity() {
		return activity;
	}
	public void setActivity(String activity) {
		this.activity = activity;
	}
	public Date getActivityTime() {
		return activityTime;
	}
	public void setActivityTime(Date activityTime) {
		this.activityTime = activityTime;
	}
	@Override
	public String toString() {
		return "PostReport [id=" + id + ", userId=" + userId + ", postId=" + postId + ", activity=" + activity
				+ ", activityTime=" + activityTime + "]";
	}
	
	
	
}
