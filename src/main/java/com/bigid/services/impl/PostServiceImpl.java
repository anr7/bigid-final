package com.bigid.services.impl;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.imageio.ImageIO;
import javax.transaction.Transactional;

import org.apache.log4j.Logger;
import org.apache.tomcat.util.codec.binary.Base64;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.bigid.business.Notification;
import com.bigid.common.enums.PostCategory;
import com.bigid.common.enums.PostType;
import com.bigid.dao.entity.Comment;
import com.bigid.dao.entity.Post;
import com.bigid.dao.entity.PostCommentVote;
import com.bigid.dao.entity.PostReplyCommentVote;
import com.bigid.dao.entity.PostReport;
import com.bigid.dao.entity.PostShared;
import com.bigid.dao.entity.PostVote;
import com.bigid.dao.entity.Reply;
import com.bigid.dao.entity.User;
import com.bigid.dao.entity.UserFollowPost;
import com.bigid.dao.entity.UserSavedPost;
import com.bigid.exceptions.BusinessException;
import com.bigid.repository.CommentRepository;
import com.bigid.repository.PostReportRepository;
import com.bigid.repository.PostRepository;
import com.bigid.repository.PostSharedRepository;
import com.bigid.repository.ReplyRepository;
import com.bigid.repository.UserFollowRepository;
import com.bigid.repository.UserRepository;
import com.bigid.repository.UserSavedRepository;
import com.bigid.services.CommentService;
import com.bigid.services.PostCommentVoteService;
import com.bigid.services.PostService;
import com.bigid.services.PostVoteService;
import com.bigid.services.ReplyVoteService;
import com.bigid.services.SecurityService;
import com.bigid.services.UserNotificationService;
import com.bigid.web.commands.CommentCommand;
import com.bigid.web.commands.CurrentFeedCommand;
import com.bigid.web.commands.CurrentFeedCommandWrapper;
import com.bigid.web.commands.HeatMapFeedCommand;
import com.bigid.web.commands.PostListCommand;
import com.bigid.web.commands.PostRequestCommand;
import com.bigid.web.commands.PostResponseCommand;
import com.bigid.web.commands.ReplyCommand;
import com.bigid.web.common.CommonUtil;
import com.bigid.web.common.Constants;

@Service("PostServiceImpl")
@Transactional
public class PostServiceImpl implements PostService {
	private final Logger logger = Logger.getLogger(PostServiceImpl.class);
	@Autowired
	private PostRepository postRepository;

	@Autowired
	private UserRepository userRepository;

	@Autowired
	SecurityService securityManager;

	@Autowired
	private CommentRepository commentRepository;

	@Autowired
	private PostSharedRepository postSharedRepository;

	@Autowired
	private ReplyVoteService replyVoteService;

	@Autowired
	private UserFollowRepository userFollowRepository;

	@Autowired
	private PostService postService;

	@Autowired
	private ReplyRepository replyRepository;
	
	@Autowired
	private PostReportRepository postReportRepository;

	@Autowired
	private PostCommentVoteService postCommentVoteService;

	@Autowired
	private UserNotificationService notificationService;

	@Autowired
	private PostVoteService postVoteService;

	@Autowired
	private CommentService commentService;

	@Autowired
	private UserSavedRepository userSavedRepository;

	@Autowired
	Environment environment;

	@Override
	public PostResponseCommand create(PostRequestCommand post) throws IOException {
		logger.info("postServiceImpl :" + post.toString());
		Post postEntity = new Post();
		BeanUtils.copyProperties(post, postEntity);
		postEntity.setStatusType("NEW");
		try {
			postEntity.setType(PostType.valueOf(post.getPostType().toUpperCase()));
		} catch (IllegalArgumentException e) {
			postEntity.setType(PostType.GENERAL);
		}
		try {
			postEntity.setCategory(PostCategory.valueOf(post.getPostCategory()));
		} catch (IllegalArgumentException e) {
			postEntity.setCategory(PostCategory.CASUAL);
		}
		postEntity.setCreatedBy(CommonUtil.getLoggedInUserId());
		postEntity.setLocation(post.getLocation());
		postEntity.setCountry(post.getCountry());
		if(post.getCountry() != null){
		  postEntity.setPopularity(popularityCount(post.getCountry()));
		}
		postEntity.setCreationTimestamp(new Date(System.currentTimeMillis()));
		postEntity.setLastModifiedTimestamp(new Date(System.currentTimeMillis()));
		postEntity.setPostCreatedTime(new Date(System.currentTimeMillis()));

		String postImage = post.getImgPath();

		if (postImage != null && postImage.length() > 0) {
			String[] parts = postImage.split(",");
			String imageString = parts[1];
			BufferedImage image = null;
			byte[] imageByte;
			imageByte = decodeImage(imageString);
			ByteArrayInputStream bis = new ByteArrayInputStream(imageByte);
			image = ImageIO.read(bis);
			bis.close();

			String str = System.currentTimeMillis() + "";
			File outputfile;
			FileOutputStream fop;
		
			if (post.getPostType().equalsIgnoreCase("VISUAL")) {
				outputfile = new File(environment.getProperty("image.path.local.diskPost") + str + ".gif");
				fop = new FileOutputStream(outputfile);
			} else {
				outputfile = new File(environment.getProperty("image.path.local.diskPost") + str + ".png");
				fop = new FileOutputStream(outputfile);
			}
			String imagePathView = environment.getProperty("image.path.viewPost") + outputfile.getName();
			//ImageIO.write(image, "gif", outputfile);
			fop.write(imageByte);
			fop.flush();
			fop.close();
			postEntity.setImgPath(imagePathView);
		}
		postRepository.save(postEntity);
		Notification notification = Notification.newInstance(CommonUtil.getLoggedInUserId(), postEntity.getId(), "POST_CREATE");
		notificationService.publish(notification);
		PostResponseCommand postResponseCommand = new PostResponseCommand();
		BeanUtils.copyProperties(postEntity,postResponseCommand);
		postResponseCommand.setPostCategory(postEntity.getCategory() == null ? null
				: postEntity.getCategory().name());
		postResponseCommand.setPostType(postEntity.getType() == null ? null : postEntity
				.getType().name());
		postResponseCommand.setUserId(postEntity.getUserId());
		return postResponseCommand;
	}
	/**
	 * Decodes the base64 string into byte array
	 *
	 * @param imageString
	 *            - a {@link java.lang.String}
	 * @return byte array
	 */
	public static byte[] decodeImage(String imageString) {
		return Base64.decodeBase64(imageString);
	}

	private Integer popularityCount(String country) {
		int popularity = 1;
		List<Post> postList = postRepository.findAll();
		
		if (postList !=null && postList.size() > 0) {
			for (Post post : postList) {
				if (country.equalsIgnoreCase(post.getCountry())) {
					popularity++;
				}
			}
		}
		if (postList !=null && postList.size() > 0) {
			for (Post post : postList) {
				if (country.equalsIgnoreCase(post.getCountry())) {
					post.setPopularity(popularity);
					postRepository.save(post);	
				}
			}
		}
		return popularity;
	}

	@Override
	public List<PostResponseCommand> findByCriteria( int pageNumber,String criteria, int size) {

		List<PostResponseCommand> postDetailCmdLst = Collections.emptyList();
			PageRequest postCriteria=null;
			if(criteria.equals(Constants.POST_DEFAULT_LISTING_CRITERIA_VOTES)){
				postCriteria = new PageRequest(pageNumber - 1, size, Direction.DESC, "voteCount");
			}
			else if(criteria.equals(Constants.POST_DEFAULT_LISTING_CRITERIA_PUSH)){
				postCriteria = new PageRequest(pageNumber - 1, size, Direction.DESC, "pushCount");
			}else if(criteria.equals(Constants.POST_DEFAULT_LISTING_CRITERIA_QUALITY)){
				postCriteria = new PageRequest(pageNumber - 1, size, Direction.DESC, "saveCount");
			}else if(criteria.equals(Constants.POST_DEFAULT_LISTING_CRITERIA_DISCUSED)){
				postCriteria = new PageRequest(pageNumber - 1, size, Direction.DESC, "commentsCount");
			}else{
				postCriteria = new PageRequest(pageNumber - 1, size, Direction.DESC, "id");
			}
			
			List<Post> postLst = postRepository.findAllByTypeNot(PostType.COMMENT,postCriteria);
			if (CollectionUtils.isEmpty(postLst) == false) {
				postDetailCmdLst = new ArrayList<PostResponseCommand>();
				for (Post post : postLst) {
					long userId = post.getCreatedBy();
					PostResponseCommand postDetailCmd = new PostResponseCommand();
					BeanUtils.copyProperties(post, postDetailCmd);

					postDetailCmd.setPostCategory(post.getCategory() == null ? null
							: post.getCategory().name());
					postDetailCmd.setPostType(post.getType() == null ? null : post
							.getType().name());
					postDetailCmd.setUserId(post.getUserId());
					buildVote(postDetailCmd, post, userId);
					buildComment(postDetailCmd, post, userId);
					isPostSavedByCurrentUser(postDetailCmd, post, userId);

					postDetailCmdLst.add(postDetailCmd);
				}
				PostListCommand postListCommand = new PostListCommand();
				postListCommand.setPosts(postDetailCmdLst);
			}
		
		return postDetailCmdLst;
	}

	public List<CommentCommand> getCommentsForPost(long postId, int pageNumber, int size) {
		List<CommentCommand> postCommentList = Collections.emptyList();
		Post post = postRepository.findPostById(postId);
		long userId = post.getCreatedBy();
		if (post != null) {
			List<Comment> commentLst = postRepository.findAllPostByIdAndType(post.getId(), PostType.COMMENT,
					new PageRequest(pageNumber - 1, size, Direction.DESC, "creationTimestamp"));
			if (CollectionUtils.isEmpty(commentLst) == false) {
				postCommentList = new ArrayList<CommentCommand>();
				for (Comment cm : commentLst) {
					CommentCommand cmCommand = new CommentCommand();
					BeanUtils.copyProperties(cm, cmCommand);
					List<Reply> replyLst = replyRepository.getRepliesByCommentId(cm.getCommentId());
					List<ReplyCommand> replyCommandLst = new ArrayList<>();
					for (Reply reply : replyLst) {
						ReplyCommand replyCommand = new ReplyCommand();
						BeanUtils.copyProperties(reply, replyCommand);
						PostReplyCommentVote postReplyCommentVote = replyVoteService.replyVote(userId, reply.getReplyId());
						if (postReplyCommentVote != null) {
							replyCommand.setPostReplyCommentVote(postReplyCommentVote);
						} else {
							postReplyCommentVote = replyVoteService.replyVote(reply.getReplyId());
							replyCommand.setPostReplyCommentVote(postReplyCommentVote);
						}
						replyCommandLst.add(replyCommand);
					}
					PostCommentVote postCommentVote = postCommentVoteService.getCommentVote(userId, cm.getCommentId());
					if (postCommentVote != null) {
						cmCommand.setCommentVoteDetails(postCommentVote);
					} else {
						postCommentVote = postCommentVoteService.getCommentVote(cm.getCommentId());
						cmCommand.setCommentVoteDetails(postCommentVote);
					}
					cmCommand.setReplies(replyCommandLst);
					postCommentList.add(cmCommand);
				}
			}
		}
		return postCommentList;
	}


	@Override
	public PostResponseCommand findPostById(long postId) {
		Post post =postRepository.findAllById(postId);
		PostResponseCommand postResponseCommand = new PostResponseCommand();
		BeanUtils.copyProperties(post, postResponseCommand);
		return postResponseCommand;
	}

	@Override
	public List<Post> getAllPostRequests() {

		List<Post> postList = postRepository.findAll();
		if (postList != null && postList.size() > 0) {
			postList.get(0).getTags();
		}
		return postList;
	}

	@Override
	public void saveCommentOnPost(Comment comment) {
		long postId = comment.getPostId();
		Post post = postRepository.findPostById(postId);
		if (post != null) {
			List<Comment> comments = new ArrayList<Comment>();
			comments.add(comment);
			post.setComment(comments);

			int commentCount = getCommentCounted(postId);
			post.setCommentsCount(commentCount);
			postRepository.save(post);
			Notification notification = Notification.newInstance(CommonUtil.getLoggedInUserId(), postId, "COMMENT");
			notificationService.publish(notification);
		} else {
			throw new BusinessException("No post found with id " + postId);
		}
	}

	private int getCommentCounted(long postId) {
		int cc = 0;
		List<Comment> commentsList = commentService.getCommentedByPostId();
		if(commentsList != null && commentsList.size()>0){
			for(Comment c : commentsList){
				if(postId == c.getPostId()){
					cc++;
				}
			}
		}
		return cc;
	}
	public void saveReplyOnComment(Long postId,Comment comment) {
		Post post = postRepository.findPostById(postId);
		comment.setPostId(postId);
		List<Comment> comments = post.getComment();
		Comment csave = null;
		for (Comment c : comments) {
			if (c.getCommentId() == comment.getCommentId()) {
				csave = c;
			}
		}
		List<Reply> replies = comment.getReplies();
		if (replies != null && replies.size() > 0) {
			List<Reply> repliesSave = new ArrayList<Reply>();
			for (Reply r : replies) {
				Reply rsave = new Reply();
				rsave.setCommentId(comment.getCommentId());
				rsave.setReplyCommentContent(r.getReplyCommentContent());
				rsave.setReplyCommentBy(r.getReplyCommentBy());
				rsave.setPostId(comment.getPostId());
				rsave.setReplyNotification(true);
				rsave.setReplyCommentTime(new Date(System.currentTimeMillis()));
				repliesSave.add(rsave);
			}
			csave.setReplies(repliesSave);
		}
		commentRepository.save(csave);
		Notification notification = Notification.newInstance(CommonUtil.getLoggedInUserId(), postId, "REPLIED");
		notificationService.publish(notification);
	}

	@Override
	public void updateTags(long postId, String tags) {
		Post postEntity = postRepository.findPostById(postId);

		Set<String> tagss = postEntity.getTags();

		Set<String> dbtags = new HashSet<String>();
		String[] tagArr = tags.split(",");
		for (String str : tagArr) {
			dbtags.add(str);
			dbtags.addAll(tagss);
		}
		postEntity.setTags(dbtags);
		postRepository.save(postEntity);

	}

	@Override
	public void deleteTags(Long postId, String tags) {
		Post postEntity = postRepository.findPostById(postId);

		Set<String> tagss = postEntity.getTags();
		String[] tagArr = tags.split(",");
		for (String str : tagArr) {
			if(tagss.contains(str)){
				tagss.remove(str);
			}
		}
		//tags after deletion
		postEntity.setTags(tagss);
		postRepository.save(postEntity);
	}
	@Override
	public Post findOne(Long postId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void save(Post post) {
		// TODO Auto-generated method stub

	}
	@Override
	public Post saveSharedPost(Long userId,Long postId) {
		Post post = postRepository.findPostById(postId);
		int shareCount=post.getSaveCount();
		User user = userRepository.findById(userId);
		Post postList = new Post();
		PostShared postShared = new PostShared();
		postShared.setUserId(userId);
		postShared.setSharedBy(user.getUsername());
		postShared.setPostId(postId);
		postShared.setStatusType("SHARED");
		postShared.setSharedTime(new Date(System.currentTimeMillis()));
		postShared.setShareCount(shareCount);
		postSharedRepository.save(postShared);
		
		post.setStatusType("SAVED");
		post.setLastModifiedTimestamp(new Date(System.currentTimeMillis()));
		post.setShareCount(shareCount);
		postRepository.save(post);

		Notification notification = Notification.newInstance(CommonUtil.getLoggedInUserId(), postId, "SHARED");
		notificationService.publish(notification);
		return postList;
	}

	@Override
	public List<Post> getSharedPost(Long postId) {
		List<Post> postList = new ArrayList<Post>();
		Post post = postRepository.findPostById(postId);
		if (post != null) {
			List<PostShared> list = postSharedRepository.findPostSavedByPostId(post.getId());
			for (PostShared postShared : list) {
				Post savedPost = postRepository.findPostById(postShared.getPostId());
				postList.add(savedPost);
			}
		}
		return postList;
	}

	@Override
	public UserSavedPost savedPost(Long userId, Long postId)  {
		User user = userRepository.findById(userId);
		Post post = postRepository.findPostById(postId);
		List<UserSavedPost> userSavedPostList = userSavedRepository.findSavePostByPostIdAndUserId(postId, userId);
		UserSavedPost savedPost = null;
		if(userSavedPostList != null && !userSavedPostList.isEmpty()){
		  savedPost = userSavedPostList.get(0);
		}
		if(savedPost == null){
			logger.debug("New Request for Saved"); 
		    savedPost = new UserSavedPost();
		    savedPost.setPostId(postId);
		    savedPost.setStatus("SAVED");
		    savedPost.setUserId(user.getId());
		    savedPost.setCreationTimestamp(new Date(System.currentTimeMillis()));
		    savedPost.setLastModifiedTimestamp(new Date(System.currentTimeMillis()));
		    userSavedRepository.save(savedPost);
		    int saveCount= post.getSaveCount();
		    saveCount = saveCount + 1;
		    post.setSaveCount(saveCount);
		    post.setStatusType("SAVED");
		    post.setLastModifiedTimestamp(new Date(System.currentTimeMillis()));
			postRepository.save(post);
		}else if(savedPost.getStatus().equalsIgnoreCase("UNSAVED")){
			logger.debug("New Request for UnSaved");
			savedPost.setStatus("SAVED");
			savedPost.setLastModifiedTimestamp(new Date(System.currentTimeMillis()));
			userSavedRepository.save(savedPost);
			int saveCount=post.getSaveCount();
			post.setSaveCount(++saveCount);
			post.setLastModifiedTimestamp(new Date(System.currentTimeMillis()));
			postRepository.save(post);
		}else if(savedPost.getStatus().equalsIgnoreCase("SAVED")){
			logger.debug("New Request for Saved");
			savedPost.setStatus("UNSAVED");
			savedPost.setLastModifiedTimestamp(new Date(System.currentTimeMillis()));
			userSavedRepository.save(savedPost);
			int saveCount=post.getSaveCount();
			post.setSaveCount(--saveCount);
			post.setLastModifiedTimestamp(new Date(System.currentTimeMillis()));
			postRepository.save(post);
		}
		savedPost.setPostCount(post.getSaveCount());
		return savedPost;
	}
	
	@Override
	public List<Post> getSavePost(Long postId) {
		List<Post> postList = new ArrayList<Post>();
		Post post = postRepository.findPostById(postId);
		if (post != null) {
			List<UserSavedPost> list = userSavedRepository.findSavePostByPostId(post.getId());
			for (UserSavedPost savePost : list) {
				Post savedPost = postRepository.findPostById(savePost.getPostId());
				postList.add(savedPost);
			}
		}
		return postList;
	}



	@Override
	public PostListCommand getAllSavedPostByUserId(long userId) {
		List<PostResponseCommand> postDetailCmdLst = new ArrayList<>();
		List<UserSavedPost> userSavedList = postService.getSavedPost(userId);
		for(UserSavedPost saved : userSavedList){
			Long postId = saved.getPostId();
			Post post = postRepository.findPostById(postId);
			if(saved.getUserId() == userId){

				if(saved.getStatus().equalsIgnoreCase("SAVED")){

					PostResponseCommand postDetailCmd = new PostResponseCommand();
					BeanUtils.copyProperties(post, postDetailCmd);

					postDetailCmd.setPostCategory(post.getCategory() == null ? null : post.getCategory().name());
					postDetailCmd.setPostType(post.getType() == null ? null : post.getType().name());
					postDetailCmd.setUserId(post.getUserId());
					buildVote(postDetailCmd, post, userId);
					buildComment(postDetailCmd, post, userId);
					isPostSavedByCurrentUser(postDetailCmd, post, userId);
					postDetailCmdLst.add(postDetailCmd);
				}
			}
		}
		PostListCommand postListCommand = new PostListCommand();
		postListCommand.setPosts(postDetailCmdLst);
		return postListCommand;		
	}

	@Override
	public Post savefollowUsers(Long userId, Long postId) {

		Post post = postRepository.findPostById(postId);
		Post postList = new Post();
		Notification notification;
		UserFollowPost userFollowPost = new UserFollowPost();

		if(userId == post.getCreatedBy()){
		}else{	
			userFollowPost.setFollower(userId);
			userFollowPost.setFollowing(post.getCreatedBy());
			userFollowPost.setFollowNotification(true);
			userFollowPost.setPostId(postId);
			userFollowPost.setSinceFollowing(new Date(System.currentTimeMillis()));
			userFollowRepository.save(userFollowPost);
		}
		post.setFollowUser(true);
		postRepository.save(post);

		notification = Notification.newInstance(CommonUtil.getLoggedInUserId(), postId, "FOLLOW");
		notificationService.publish(notification);

		return postList;
	}
	@Override
	public List<UserSavedPost> getSavedPost(Long userId) {

		List<UserSavedPost> postSavedList = new ArrayList<UserSavedPost>();
		User user = userRepository.findById(userId);
		if (user != null) {
			List<UserSavedPost> list = userSavedRepository.findSavedPostByUserId(user.getId());
			for (UserSavedPost savedPost : list) {
				UserSavedPost userSavedPost = userSavedRepository.findOne(savedPost.getId());
				postSavedList.add(userSavedPost);
			}
		} // else block will throw the exception
		return postSavedList;
	}

	@Override
	public List<Post> getAllSavedPostRequests(Long userId) {

		List<Post> postList = new ArrayList<Post>();
		User user = userRepository.findById(userId);
		if (user != null) {
			List<PostShared> list = postSharedRepository.findPostSharedByUserId(user.getId());
			for (PostShared postShared : list) {
				Post post = postRepository.findPostById(postShared.getPostId());
				postList.add(post);
			}
		} // else block will throw the exception
		return postList;
	}

	@Override
	public List<PostShared> getAllSharedPost(Long postId) {
		List<PostShared> postList = new ArrayList<PostShared>();
		PostShared postShared = postSharedRepository.findOne(postId);
		if (postShared != null) {
			List<PostShared> list = postSharedRepository.findPostSavedByPostId(postShared.getId());
			for (PostShared postShare : list) {
				PostShared savedPost = postSharedRepository.findOne(postShare.getPostId());
				postList.add(savedPost);
			}
		}
		return postList;
	}

	public Post enableNotification(Long postid) {
		Post post = postRepository.findPostById(postid);
		if (post == null) {
			return null;
			// we will throw exeption
		} else {
			post.setPostNotification(true);
			post.setLastModifiedTimestamp(new Date(System.currentTimeMillis()));
			postRepository.save(post);
			return post;
		}
	}
	public Post disableNotification(Long postid) {
		Post post = postRepository.findPostById(postid);
		if (post == null) {
			return null;
			// we will throw exeption
		} else {
			post.setPostNotification(false);
			post.setLastModifiedTimestamp(new Date(System.currentTimeMillis()));
			postRepository.save(post);
			return post;
		}
	}

	@Override
	public List<User> getFollowers(Long id) {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public PostListCommand getAllPosts(long userId) {
		List<PostResponseCommand> postDetailCmdLst = new ArrayList<>();
		User user = userRepository.findById(userId);
		List<Post> postLst = postService.getAllPostRequests();
		for (Post post : postLst) {
			
			PostResponseCommand postDetailCmd = new PostResponseCommand();
			BeanUtils.copyProperties(post, postDetailCmd);

			postDetailCmd.setPostCategory(post.getCategory() == null ? null : post.getCategory().name());
			postDetailCmd.setPostType(post.getType() == null ? null : post.getType().name());
			postDetailCmd.setUserId(post.getUserId());
			buildVote(postDetailCmd, post, userId);
			buildComment(postDetailCmd, post, userId);
			setImagePath(postDetailCmd,post,user);
			isPostSavedByCurrentUser(postDetailCmd, post, userId);
			postDetailCmdLst.add(postDetailCmd);
		}

		PostListCommand postListCommand = new PostListCommand();
		postListCommand.setPosts(postDetailCmdLst);
		return postListCommand;
	}
	
	private void setImagePath(PostResponseCommand postDetailCmd,Post post,User user){
		if(post.getImgPath() == null || "".equalsIgnoreCase(post.getImgPath())){
			if(post.getCreatedBy() == user.getId()){
				postDetailCmd.setImgPath(user.getAvatarImgPath());
			}else{
				User user2 = userRepository.findById(post.getCreatedBy());
				postDetailCmd.setImgPath(user2.getAvatarImgPath());
			}
		}
	}
	
	private void isPostSavedByCurrentUser(PostResponseCommand postDetailCmd,Post post,Long userId){
		List<UserSavedPost> userSavedPostList = userSavedRepository.findSavePostByPostIdAndUserId(post.getId(), userId);
		UserSavedPost savedPost = null;
		if(userSavedPostList != null && !userSavedPostList.isEmpty()){
		  savedPost = userSavedPostList.get(0);
		}
		if(savedPost == null || savedPost.getStatus().equalsIgnoreCase("UNSAVED")){
			postDetailCmd.setPostSavedByLoggedInUser(false);
		}else {
			postDetailCmd.setPostSavedByLoggedInUser(true);
		}	
	}
	
	private void buildVote(PostResponseCommand postDetailCmd,Post post,Long userId){
		PostVote voteDetail = postVoteService.getVote(userId, post.getId());
		if (voteDetail != null) {
			postDetailCmd.setVoteDetails(voteDetail);
		} else {
			voteDetail = postVoteService.getVote(post.getId());
			postDetailCmd.setVoteDetails(voteDetail);
		}
	}
	private void buildComment(PostResponseCommand postDetailCmd,Post post,Long userId){
		List<CommentCommand> cmCommandLst = new ArrayList<>();
		List<Comment> commentLst = commentRepository.getCommentsByPostId(post.getId());
		for (Comment cm : commentLst) {
			CommentCommand cmCommand = new CommentCommand();
			BeanUtils.copyProperties(cm, cmCommand);
			List<Reply> replyLst = replyRepository.getRepliesByCommentId(cm.getCommentId());
			List<ReplyCommand> replyCommandLst = new ArrayList<>();
			for (Reply reply : replyLst) {
				ReplyCommand replyCommand = new ReplyCommand();
				BeanUtils.copyProperties(reply, replyCommand);
				PostReplyCommentVote postReplyCommentVote = replyVoteService.replyVote(userId, reply.getReplyId());
				if (postReplyCommentVote != null) {
					replyCommand.setPostReplyCommentVote(postReplyCommentVote);
				} else {
					postReplyCommentVote = replyVoteService.replyVote(reply.getReplyId());
					replyCommand.setPostReplyCommentVote(postReplyCommentVote);
				}
				replyCommandLst.add(replyCommand);
			}
			PostCommentVote postCommentVote = postCommentVoteService.getCommentVote(userId, cm.getCommentId());
			if (postCommentVote != null) {
				cmCommand.setCommentVoteDetails(postCommentVote);
			} else {
				postCommentVote = postCommentVoteService.getCommentVote(cm.getCommentId());
				cmCommand.setCommentVoteDetails(postCommentVote);
			}
			cmCommand.setReplies(replyCommandLst);
			cmCommandLst.add(cmCommand);
		}
		postDetailCmd.setComments(cmCommandLst);
	}


	@Override
	public PostListCommand getAllPostList() {

		List<PostResponseCommand> postDetailCmdLst = new ArrayList<>();
		List<Post> postLst = postService.getAllPostRequests();
		for (Post post : postLst) {
			long userId = post.getCreatedBy();
			PostResponseCommand postDetailCmd = new PostResponseCommand();
			BeanUtils.copyProperties(post, postDetailCmd);

			postDetailCmd.setPostCategory(post.getCategory() == null ? null : post.getCategory().name());
			postDetailCmd.setPostType(post.getType() == null ? null : post.getType().name());
			postDetailCmd.setUserId(post.getUserId());
			buildVote(postDetailCmd, post, userId);
			buildComment(postDetailCmd, post, userId);
			isPostSavedByCurrentUser(postDetailCmd, post, userId);
			postDetailCmdLst.add(postDetailCmd);
		}

		PostListCommand postListCommand = new PostListCommand();
		postListCommand.setPosts(postDetailCmdLst);
		return postListCommand;
	}


	@Override
	public PostListCommand getAllSharedPostByUserId(long userId) {
		List<PostResponseCommand> postDetailCmdLst = new ArrayList<>();
		List<Post> postLst = postService.getAllSavedPostRequests(userId);

		for (Post post : postLst) {
			if (post.getStatusType().equalsIgnoreCase("SAVED")) {
				PostResponseCommand postDetailCmd = new PostResponseCommand();
				BeanUtils.copyProperties(post, postDetailCmd);

				postDetailCmd.setPostCategory(post.getCategory() == null ? null : post.getCategory().name());
				postDetailCmd.setPostType(post.getType() == null ? null : post.getType().name());
				postDetailCmd.setUserId(post.getUserId());
				buildVote(postDetailCmd, post, userId);
				buildComment(postDetailCmd, post, userId);
				isPostSavedByCurrentUser(postDetailCmd, post, userId);
				postDetailCmdLst.add(postDetailCmd);
			}
		}
		PostListCommand postListCommand = new PostListCommand();
		postListCommand.setPosts(postDetailCmdLst);
		return postListCommand;
	}


	@Override
	public PostListCommand getPostDetailsByUserId(long userId, long postId) {
		List<PostResponseCommand> postDetailCmdLst = new ArrayList<>();
		Post post = postRepository.findPostById(postId);
		if(post != null) {
			PostResponseCommand postDetailCmd = new PostResponseCommand();
			BeanUtils.copyProperties(post, postDetailCmd);

			postDetailCmd.setPostCategory(post.getCategory() == null ? null : post.getCategory().name());
			postDetailCmd.setPostType(post.getType() == null ? null : post.getType().name());
			postDetailCmd.setUserId(post.getUserId());
			buildVote(postDetailCmd, post, userId);
			buildComment(postDetailCmd, post, userId);
			isPostSavedByCurrentUser(postDetailCmd, post, userId);
			postDetailCmdLst.add(postDetailCmd);
		}
		PostListCommand postListCommand = new PostListCommand();
		postListCommand.setPosts(postDetailCmdLst);
		return postListCommand;
	}

	@Override
	public PostListCommand getAllPostCreatedUserId(long userId) {

		List<PostResponseCommand> postDetailCmdLst = new ArrayList<>();
		List<Post> postLst = postService.getAllPostRequests();
		for (Post post : postLst) {
			if(post.getCreatedBy() == userId){
				PostResponseCommand postDetailCmd = new PostResponseCommand();
				BeanUtils.copyProperties(post, postDetailCmd);

				postDetailCmd.setPostCategory(post.getCategory() == null ? null : post.getCategory().name());
				postDetailCmd.setPostType(post.getType() == null ? null : post.getType().name());
				postDetailCmd.setUserId(post.getUserId());
				
				buildVote(postDetailCmd, post, userId);
				buildComment(postDetailCmd, post, userId);
				isPostSavedByCurrentUser(postDetailCmd, post, userId);
				postDetailCmdLst.add(postDetailCmd);
			}
		}
		PostListCommand postListCommand = new PostListCommand();
		postListCommand.setPosts(postDetailCmdLst);
		return postListCommand;
	}
	
	/*@Override
	public List<PostResponseCommand> getAllPostCreatedUserId(long userId,  int pageNumber,String showCriteria, int size) {

		List<PostResponseCommand> postDetailCmdLst = Collections.emptyList();
		
		PageRequest postCriteria=null;
		if(showCriteria.equals(Constants.POST_DEFAULT_LISTING_CRITERIA_MYPOST)){
			postCriteria = new PageRequest(pageNumber - 1, size, Direction.DESC, "createdBy");
		}
			List<Post> postLst = postRepository.findAllByTypeNot(PostType.COMMENT,
					postCriteria);
			if (CollectionUtils.isEmpty(postLst) == false) {
				postDetailCmdLst = new ArrayList<PostResponseCommand>();
				for (Post post : postLst) {
					if(post.getCreatedBy() == userId){
					PostResponseCommand postDetailCmd = new PostResponseCommand();
					BeanUtils.copyProperties(post, postDetailCmd);

					postDetailCmd.setPostCategory(post.getCategory() == null ? null
							: post.getCategory().name());
					postDetailCmd.setPostType(post.getType() == null ? null : post
							.getType().name());
					postDetailCmd.setUserId(post.getUserId());
					buildVote(postDetailCmd, post, userId);
					buildComment(postDetailCmd, post, userId);
					isPostSavedByCurrentUser(postDetailCmd, post, userId);

					postDetailCmdLst.add(postDetailCmd);
				}
				}
				PostListCommand postListCommand = new PostListCommand();
				postListCommand.setPosts(postDetailCmdLst);
			}
		
		return postDetailCmdLst;
	}*/
	
	@Override
	public Object getAllCurrentFeeds() {
		CurrentFeedCommandWrapper currFeedComwrap = new CurrentFeedCommandWrapper();
		List<Post> postList = postRepository.findAll();
		ArrayList<CurrentFeedCommand> currentFeedCommandList = new ArrayList<CurrentFeedCommand>();
		if (postList != null && postList.size() > 0) {
			for (Post cf : postList) {
				CurrentFeedCommand currentFeedCommand = new CurrentFeedCommand();
				BeanUtils.copyProperties(cf, currentFeedCommand);
				currentFeedCommand.getCoordinates().add(currentFeedCommand.getLongitude());
				currentFeedCommand.getCoordinates().add(currentFeedCommand.getLatitude());
				currentFeedCommandList.add(currentFeedCommand);
			}
		}
		currFeedComwrap.setFeedlocations(currentFeedCommandList);
		return currFeedComwrap;
	}

	@Override
	public Object getHeatMapFeed() {
		List<HeatMapFeedCommand> postResp = new ArrayList<HeatMapFeedCommand>();
		int count = 0;
		Set<String> countrySet = new HashSet<>();
		List<Post> postList = postRepository.findAll();
		if (postList != null && postList.size() > 0) {
			for (Post hmf : postList) {
				if (!countrySet.contains(hmf.getCountry())) {
					countrySet.add(hmf.getCountry());
					HeatMapFeedCommand temp = new HeatMapFeedCommand();
					temp.setCountry(hmf.getCountry());
					temp.setPopularity(hmf.getPopularity());
					postResp.add(count, temp);
					count++;
				}
			}
		}
		return postResp;
	}

	@Override
	public Object getCommentsReply(Long postId) {
		Post post = postRepository.findPostById(postId);
		long userId = post.getCreatedBy();
		List<CommentCommand> cmCommandLst = new ArrayList<>();
		List<Comment> commentLst = commentRepository.getCommentsByPostId(post.getId());
		for (Comment cm : commentLst) {
			CommentCommand cmCommand = new CommentCommand();
			BeanUtils.copyProperties(cm, cmCommand);
			List<Reply> replyLst = replyRepository.getRepliesByCommentId(cm.getCommentId());
			List<ReplyCommand> replyCommandLst = new ArrayList<>();
			for (Reply reply : replyLst) {
				ReplyCommand replyCommand = new ReplyCommand();
				BeanUtils.copyProperties(reply, replyCommand);
				PostReplyCommentVote postReplyCommentVote = replyVoteService.replyVote(userId, reply.getReplyId());
				if (postReplyCommentVote != null) {
					replyCommand.setPostReplyCommentVote(postReplyCommentVote);
				} else {
					postReplyCommentVote = replyVoteService.replyVote(reply.getReplyId());
					replyCommand.setPostReplyCommentVote(postReplyCommentVote);
				}

				replyCommandLst.add(replyCommand);
			}
			PostCommentVote postCommentVote = postCommentVoteService.getCommentVote(userId, cm.getCommentId());
			if (postCommentVote != null) {
				cmCommand.setCommentVoteDetails(postCommentVote);
			} else {
				postCommentVote = postCommentVoteService.getCommentVote(cm.getCommentId());
				cmCommand.setCommentVoteDetails(postCommentVote);
			}
			cmCommand.setReplies(replyCommandLst);
			cmCommandLst.add(cmCommand);
		}
		return cmCommandLst;
	}
	@Override
	public void read(Long userId, Long postId) {
		this.logger.info("Post : [" + postId + "] read by user : [ " + userId +" ]");
		User user = userRepository.findById(userId); //TODO read from logged in user
		Post post = postRepository.findPostById(postId);
		
		List<PostReport> postReportList = postReportRepository.findPostReportByPostIdAndUserId(postId, userId);
		PostReport report = null;
		if(postReportList == null || postReportList.isEmpty()){
			report = new PostReport();
			report.setActivity("READ");
			report.setPostId(post.getId());
			report.setUserId(user.getId());
			report.setActivityTime(new Date());
			postReportRepository.save(report);
			
		}
		this.logger.info("Post : [" + postId + "] read by user : [ " + userId +" ] successfully Logged");
	}
	
	@Override
	public HashMap<String, Integer> getTheTagsPriority() {
		HashMap<String, Integer> tasgCount = new HashMap<>();
		List<Post> postLst = postRepository.findAll();
		if(postLst.size()>0 && postLst != null){
			for (Post post : postLst) {
				Set<String> tagss = post.getTags();
				for (String word : tagss) {
					if (tasgCount.containsKey(word)) {
						tasgCount.put(word, tasgCount.get(word) + 1);
					} else {
						tasgCount.put(word, 1);
					}
				}
			}
		}
		return tasgCount;
	}
	
	@Override
	public List<PostResponseCommand> getPostsCriteria(int pageNumber, String c1,String c2,String c3,String c4, int size) {

		List<PostResponseCommand> postDetailCmdLst = Collections.emptyList();
		PageRequest postCriteria=null;
		String[] criteraList={};
		List<String> list = new ArrayList<String>();
		list.add(c1);
	 if(!c2.equals(Constants.POST_DEFAULT_LISTING_CRITERIA_NOTFOUND)){
			list.add(c2);
		}
	 if(!c3.equals(Constants.POST_DEFAULT_LISTING_CRITERIA_NOTFOUND)){
			list.add(c3);
		}
	 if(!c4.equals(Constants.POST_DEFAULT_LISTING_CRITERIA_NOTFOUND)){
		 list.add(c4);
		}
	 postCriteria=new PageRequest(pageNumber - 1, size, Direction.DESC,list.toArray(criteraList));
		List<Post> postLst = postRepository.findAllByTypeNot(PostType.COMMENT,postCriteria);
		if (CollectionUtils.isEmpty(postLst) == false) {
			postDetailCmdLst = new ArrayList<PostResponseCommand>();
			for (Post post : postLst) {
				long userId = post.getCreatedBy();
				PostResponseCommand postDetailCmd = new PostResponseCommand();
				BeanUtils.copyProperties(post, postDetailCmd);

				postDetailCmd.setPostCategory(post.getCategory() == null ? null
						: post.getCategory().name());
				postDetailCmd.setPostType(post.getType() == null ? null : post
						.getType().name());
				postDetailCmd.setUserId(post.getUserId());
				buildVote(postDetailCmd, post, userId);
				buildComment(postDetailCmd, post, userId);
				isPostSavedByCurrentUser(postDetailCmd, post, userId);

				postDetailCmdLst.add(postDetailCmd);
			}
			PostListCommand postListCommand = new PostListCommand();
			postListCommand.setPosts(postDetailCmdLst);
		}

		return postDetailCmdLst;
	}
}