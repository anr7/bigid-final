package com.bigid.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.bigid.dao.entity.UserSavedPost;

/**
 * Spring Data JPA focuses on using JPA to store data in a relational database
 * UserSavedRepository use to save all the user related data in database
 * @author satish
 *
 */
@Repository
public interface UserSavedRepository extends JpaRepository<UserSavedPost, Long> {

	/**This API is used to find the Save post by UserId.
	 * @param userId
	 * @return
	 */
	List<UserSavedPost> findSavedPostByUserId(Long userId);

	/**This API is used to find the Save post by PostId
	 * @param postId
	 * @return
	 */
	List<UserSavedPost> findSavePostByPostId(Long postId);
	
	/**This API is used to find the save post by userId and postId
	 * @param postId
	 * @param userId
	 * @return
	 */
	List<UserSavedPost> findSavePostByPostIdAndUserId(Long postId,Long userId);

}
