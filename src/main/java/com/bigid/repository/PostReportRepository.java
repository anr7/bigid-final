package com.bigid.repository;



import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.bigid.dao.entity.PostReport;


/**
 * Spring Data JPA focuses on using JPA to store data in a relational database
 * PostReportRepository use to save all the user related data in database
 * @author satish
 *
 */
@Repository
public interface PostReportRepository extends JpaRepository<PostReport, Long> {


	/**This API is used to find the post read report by userId and postId.
	 * @param postId
	 * @param userId
	 * @return
	 */
	List<PostReport> findPostReportByPostIdAndUserId(Long postId, Long userId);

	
}
