
function preLoginService(){
	var self= this;
	 this.loggedInUserName = "";
	 this.setUserName = function(name) {
		 this.loggedInUserName=name;
		 localStorage.setItem("app.userName", name);
	 }
	 
	 this.getUserName = function() {
		 return this.loggedInUserName || localStorage.getItem("app.userName");
	 };
	
	 (function(){
		 self.loggedInUserName = self.getUserName();
	 })();
}

angular.module('tbidApp')
       .service('preLoginService', preLoginService);