angular
  .module('tbidApp')
  .factory('postUrlService', PostUrlService);


PostUrlService.$inject = ['urlService'];

function PostUrlService(urlService) {

  return {
    getAllPostUrl: function (userid) {
      return urlService.getSecurePath()+"/post/"+userid+"/list"
    },
    getPostsByScrollUrl: function (userid,pageNo,noOfPosts,criteria) {
    	criteria = criteria || 'latest';
        return urlService.getSecurePath()+"/post/show?page="+pageNo+"&size="+noOfPosts+"&criteria="+criteria;
      },
    getFollowUserUrl : function(data){
    	return urlService.getSecurePath()+"/post/"+data.userId+"/"+data.postId;
    },
    getPostNotificationUrl : function(data){
    	if(data.enableNotificationFromUser){
    		return urlService.getSecurePath()+"/post/enablenotification/"+data.postId;
    	}else{
    		return urlService.getSecurePath()+"/post/disablenotification/"+data.postId;
    	}
    },
    getSavedPostsUrl : function(userId,pageNo,noOfPosts){
    	return urlService.getSecurePath()+"/post/saved/"+userId+"?page="+pageNo+"&size="+noOfPosts
    },
    getSavePostUrl : function(userId,postId){
    	return urlService.getSecurePath()+"/post/save/"+userId+"/"+postId;
    },

    getPostReportUrl : function(data){
        	return urlService.getSecurePath()+"/post/read/"+data.userId+"/"+data.postId;
    },
    myPostsUrl : function(userId,pageNo,noOfPosts){
    	return urlService.getSecurePath() + "/post/"+userId+"/allpost?page="+pageNo+"&size="+noOfPosts;
    },
    updatePostTags : function(postId){
    	return urlService.getSecurePath() + "/post/"+postId +"/tags";
    },
    getTags: function () {
        return urlService.getSecurePath()+"/post/tags";
    },
    getTopPosts: function () {
         return urlService.getSecurePath()+"/post/showpost?page=1&size=3&c1=voteCount&c2=pushCount&c3=saveCount";
    }
  }
}
