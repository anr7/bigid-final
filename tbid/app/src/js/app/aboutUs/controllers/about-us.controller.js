function aboutUsController($scope, myProfileService, $rootScope,$location,loginService,preLoginService) {
		
	$scope.rootData.error = "";

	$scope.reloadMainWall = function() {
		$rootScope.showSearchBoxFlag = true;
		$scope.rootData.error = "";
		$location.path('/');
	}
	

	
	$scope.$on("logout", function(){
	    $scope.data  = {};
	    $scope.showLoader();
	    $location.path('/login');
	    $scope.hideLoader();
	  });
  
};

aboutUsController.$inject = [ '$scope', 'myProfileService', '$rootScope',
		'$location', 'loginService','preLoginService'];

angular.module('tbidApp').controller('aboutUsController',
		aboutUsController);