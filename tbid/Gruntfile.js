module.exports = function(grunt) {

  'use strict';

  // Project configuration.
  grunt.initConfig({

    pkg: grunt.file.readJSON('package.json'),

    clean: {
      build:[
        "target"
      ],
      postbuild:['target/js/<%= pkg.name %>.js', 'target/js/lib', 'target/src/js']
    },

    concat: {
      options: {
        separator: ';'
      },
      target: {
        src: ['src/js/app/**/*.js'],
        dest: 'target/js/<%= pkg.name %>.js'
      }
    },

    jshint: {
      files: ['Gruntfile.js', 'src/js/app/**/*.js'],
      options: {
        // options here to override JSHint defaults
        globals: {
          jQuery: true,
          console: true,
          module: true,
          document: true
        }
      }
    },
    useref: {
      // specify which files contain the build blocks
      html: 'target/*.html',
      // explicitly specify the temp directory you are working in
      // this is the the base of your links ( '/' )
      temp: 'target'
    },

    copy: {
      main:{
        files: [
          {expand: true, cwd: 'css/', src: ['**/*.*'], dest: 'target/css'},
          {expand: true, cwd: 'css/fonts', src: ['**/*.*'], dest: 'target/css/fonts'},
          {expand: true, flatten: true, cwd: 'images/', src: ['**'], dest: 'target/images'},
          {expand: true, cwd: 'js/lib', src: ['**/*.js'], dest: 'target/js/lib'},
          {expand: true, cwd: 'src/js/app', src: ['**/*.js'], dest: 'target/src/js/app'},
          {expand: true, cwd: 'views/templates', src: ['**/*.*'], dest: 'target/views/templates'},
          {expand: true, cwd: '', src: ['index.html'], dest: 'target'},
          {expand: true, cwd: 'views', src: ['login.html','register.html','main.html'], dest: 'target/views'},
          {expand: true, cwd: 'properties', src: ['**/*.*'], dest: 'target/properties'}
        ],

        options: {
          files: ['package.json'], // list of files to add build info
          buildField: 'buildInfo' // add a 'buildInfo' element to the package.json containing the build info
        }
      },
      postBuildTraining: {
        files:[

          //  copy training folder for training environment alone
          {expand: true, cwd: 'training', src: ['**/*.*'], dest: 'target/training'},
          {expand: true, cwd: 'traininghtml', src: ['**/*.*'], dest: 'target/traininghtml'}
        ]
      },
      postBuild: {
        files:[

          //  Copy IE specific files. These are already minified
          {expand: true, cwd: 'js/lib', src: ['html5shiv.min.js', 'respond.min.js'], dest: 'target/js/lib'},

          //  jQuery is imported within the Thistle redirect HTML, so we need to provide a copy for successful import
          {expand: true, cwd: 'js/lib/jquery', src: ['jquery-1.*.js'], dest: 'target/js/lib/jquery'},

          //  Copy the lib.js JavaScript file so that it is accessible from the redirect files
          {expand: true, cwd: 'js/lib', src: ['lib.js'], dest: 'target/js/lib/'}

        ]
      }
    },

    uglify: {
      options: {
        // the banner is inserted at the top of the output
        banner: '/*! <%= grunt.template.today("dd-mm-yyyy HH:MM:ss") %> */\n'
      }
    },

    watch: {
      files: ['<%= jshint.files %>'],
      tasks: ['jshint', 'uglify']
    },


    validation: {
      options: {
        reset: grunt.option('reset') || false,
        proxy: 'http://proxy.intra.bt.com:8080',
        stoponerror: false
      },
      files: {
        src: ['templates/**/*.html',
          'index.html',
          'appindex.html']
      }
    },

    htmlangular: {
      options: {
        angular: true,
        w3cproxy: "http://proxy.intra.bt.com:8080",
        tmplext: 'html',
        customtags: [
          'loading'
        ],
        customattrs: [
          'datepicker-popup'
        ],
        relaxerror: [
          'The frameborder attribute on the iframe element is obsolete. Use CSS instead.'
        ],
        reportpath: 'test/out/html-angular-validate-report.json',
        reportCheckstylePath: 'test/out/html-angular-validate-report-checkstyle.xml'
      },
      files: {
        src: ['templates/**/*.html', '*.html']
      }
    }
  });
  //if we are using the grunt-userref then we can omit below lib or package.
  //grunt-contrib-concat
  //grunt-contrib-uglify
  //grunt-css

  grunt.loadNpmTasks('grunt-contrib-clean');
  grunt.loadNpmTasks('grunt-contrib-jshint');
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-concat');
  grunt.loadNpmTasks('grunt-contrib-copy');
  grunt.loadNpmTasks('grunt-contrib-watch');
  //grunt.loadNpmTasks('grunt-useref');
  //grunt.loadNpmTasks('grunt-jenkins-build-info');
  //grunt.loadNpmTasks('grunt-html-validation');
  //grunt.loadNpmTasks('grunt-html-angular-validate');
  //grunt.loadNpmTasks('grunt-sonar-runner');
  grunt.loadNpmTasks('grunt-contrib-htmlmin');
  //grunt.loadNpmTasks('grunt-uncss');


  //To build bpta-client
  grunt.registerTask('default', ['clean:build', 'copy:main', 'concat', 'copy:postBuild']);
  //To build ee client
  grunt.registerTask('eeDefault', ['clean:build', 'copy:main_ee', 'useref', 'concat', 'uglify', 'clean:postbuild', 'copy:postBuild','cssmin','uncss']);
  grunt.registerTask('training', ['clean:build', 'jshint', 'csslint', 'copy:main', 'useref', 'concat', 'uglify', 'clean:postbuild', 'copy:postBuild', 'copy:postBuildTraining']);
  grunt.registerTask('buildNoCodeAnalysis', ['clean:build', 'copy:main', 'useref', 'concat', 'uglify', 'clean:postbuild', 'copy:postBuild']);
  grunt.registerTask('buildAndWatch', ['clean:build', 'jshint', 'copy:main', 'useref', 'concat', 'uglify', 'clean:postbuild', 'copy:postBuild', 'watch']);
  grunt.registerTask('codeAnalysis', ['sonarRunner:analysis']);

};
